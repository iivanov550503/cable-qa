const int RJ45_OUTPUT_1 = 2;
const int RJ45_OUTPUT_8 = 9;

void setup() {
  for (int i = RJ45_OUTPUT_1; i <= RJ45_OUTPUT_8; ++i) {
    pinMode(i, OUTPUT);
    pinMode(i, HIGH);
  }
}

void loop() {
}
