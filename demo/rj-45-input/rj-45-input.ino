const int RJ45_INPUT_1  = 2;
const int RJ45_INPUT_8  = 9;

void setup() {
  // put your setup code here, to run once:

  for (int i = RJ45_INPUT_1; i <= RJ45_INPUT_8; ++i) {
    pinMode(i, INPUT_PULLUP);
  }

  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println(F("-----Start-----"));
}

void loop() {
  for (int i = RJ45_INPUT_1; i <= RJ45_INPUT_8; ++i) {
    Serial.print(String(" ") + digitalRead(i));
  }
  Serial.println("");

  delay(1000);
}
