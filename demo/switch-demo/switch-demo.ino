#include <Indicator.h>

const int BUTTON_PIN = 2;
const int INDICATOR_PIN = 13;
const int SWITCH_PIN = A5;

Indicator green = Indicator(HIGH, INDICATOR_PIN);

void click_button() {
  green.toggle();
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), click_button, FALLING);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (green.getState()) {
    auto input = analogRead(SWITCH_PIN);
    Serial.println("Mode: " + String(map(input, 0, 1023, 0, 2)));
    auto start = millis();
    auto break_exit = false;
    while (millis() - start < 1000) {
      if (!green.getState()) {
        break_exit = true;
        break;
      }
    }
    if (!break_exit) {
      green.reset();
    }
  }
}
