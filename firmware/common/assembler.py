import argparse, os, re, shutil, glob


def set_argument_parser():
    parser = argparse.ArgumentParser(description='Files flatter')
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    parser.add_argument('-c', '--clear', action='store_const', const=True)
    parser.add_argument('-i', '--input', type=str, nargs='+')
    parser.add_argument('-o', '--output', type=str, default='dist')
    parser.add_argument('-s', '--skip', type=str, nargs='*', default=[])
    parser.add_argument('-v', '--verbose', action='store_true', help='Verbose output')
    return parser.parse_args()


if __name__ == "__main__":
    args = set_argument_parser()
    regex = re.compile(r'(")(?:(?:\.\.|\.|\w+)/)+(.+?\.h)\1', re.IGNORECASE)
    output = args.output
    if not os.path.exists(output):
        os.makedirs(output)
    elif args.clear:
        for file in [entry for entry in os.listdir(output) if os.path.isfile(entry)]:
            os.remove(os.path.join(output, file))
        if args.verbose:
            print('Output directory "{}" cleared.'.format(output))

    for inputDir in args.input:
        for folder, subs, files in os.walk(inputDir):
            subs[:] = [d for d in subs if d not in args.skip]
            for file in [f for f in files if f.endswith(('.h', '.cpp'))]:
                in_path = os.path.join(folder, file)
                out_path = os.path.join(output, file)
                if args.verbose:
                    print('File: {:<50} moved to {:<30}'.format(in_path, out_path))
                with open(in_path, 'rt') as f_in:
                    with open(out_path, 'wt+') as f_out:
                        for line in f_in:
                            if not line.startswith('#include'):
                                f_out.write(line)
                            else:
                                f_out.write(regex.sub('\\1\\2\\1', line))
