//
// Created by ilya on 11/5/17.
//

#include "DigitalDeviceBase.h"

DigitalDeviceBase::DigitalDeviceBase(uint8_t activeLevel) :activeLevel(activeLevel) {}

uint8_t DigitalDeviceBase::getActiveLevel(SignalLevel value) const noexcept {
    return (uint8_t) (value == SignalLevel::ACTIVE ? activeLevel : !activeLevel);
}
