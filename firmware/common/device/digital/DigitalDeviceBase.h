//
// Created by ilya on 11/5/17.
//

#ifndef CABLE_QA_COMMON_COMMON_DIGITAL_DEVICE_BASE_H
#define CABLE_QA_COMMON_COMMON_DIGITAL_DEVICE_BASE_H

#include <Arduino.h>

enum class SignalLevel {
    ACTIVE,
    INACTIVE
};

class DigitalDeviceBase {
public:
    explicit DigitalDeviceBase(uint8_t activeLevel);

protected:
    const uint8_t activeLevel;
    uint8_t getActiveLevel(SignalLevel value) const noexcept;
};


#endif //CABLE_QA_COMMON_COMMON_DIGITAL_DEVICE_BASE_H
