//
// Created by ilya on 11/12/17.
//

#include "Switch.h"

const long Switch::UPPER_INPUT_THRESHOLD = 1023;

Switch::Switch(const uint8_t pin, const int modes) noexcept
        : upperOutputThreshold(modes), pin(pin) {
}

long Switch::getMode() const noexcept {
    auto input = analogRead(pin);
    return map(input, 0, UPPER_INPUT_THRESHOLD, 0, upperOutputThreshold);
}
