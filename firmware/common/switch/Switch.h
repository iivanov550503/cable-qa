//
// Created by ilya on 11/12/17.
//

#ifndef COMMON_SWITCH_H
#define COMMON_SWITCH_H

#include <Arduino.h>

class Switch {
public:
    explicit Switch(uint8_t pin, int modes) noexcept;

    long getMode() const noexcept;

private:
    static const long UPPER_INPUT_THRESHOLD;

    const long upperOutputThreshold;
    const uint8_t pin;
};


#endif //COMMON_SWITCH_H
