//
// Created by root on 10/27/17.
//

#ifndef CABLE_QA_COMMON_MEDIATOR_H
#define CABLE_QA_COMMON_MEDIATOR_H

#include <Arduino.h>
#include "../device/digital/DigitalDeviceBase.h"

class Adapter : public DigitalDeviceBase {
public:
    Adapter(uint8_t activeLevel, unsigned long timeout) noexcept;

    virtual ~Adapter() = default;

    virtual void pulse(uint8_t contact) const noexcept = 0;
    virtual SignalLevel readLevel(uint8_t contact) const noexcept = 0;
    virtual void writeLevel(uint8_t contact, SignalLevel signalLevel) const noexcept = 0;
    virtual bool waitFor(uint8_t contact, SignalLevel signalLevel) const noexcept = 0;
    virtual uint8_t getPhysicalPin(uint8_t contact) const noexcept = 0;
    virtual void highZ(uint8_t contact) const noexcept = 0;

protected:
    const unsigned long timeout;
};


#endif //CABLE_QA_COMMON_MEDIATOR_H
