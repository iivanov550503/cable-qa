//
// Created by root on 10/27/17.
//

#include "Adapter.h"

Adapter::Adapter(const uint8_t activeLevel, const unsigned long timeout) noexcept
        : DigitalDeviceBase(activeLevel), timeout(timeout) {
}
