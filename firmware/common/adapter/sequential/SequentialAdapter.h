//
// Created by ilya on 10/29/17.
//

#ifndef CABLE_QA_COMMON_SEQUENTIAL_ADAPTER_H
#define CABLE_QA_COMMON_SEQUENTIAL_ADAPTER_H

#include "../Adapter.h"

class SequentialAdapter: public Adapter {
public:
    static const unsigned long DEFAULT_TIMEOUT;

    SequentialAdapter(uint8_t startPin, uint8_t activeLevel, unsigned long timeout = DEFAULT_TIMEOUT) noexcept;

    void pulse(uint8_t contact) const noexcept override;

    SignalLevel readLevel(uint8_t contact) const noexcept override;

    void writeLevel(uint8_t contact, SignalLevel signalLevel) const noexcept override;

    bool waitFor(uint8_t contact, SignalLevel signalLevel) const noexcept override;

    uint8_t getPhysicalPin(uint8_t contact) const noexcept override;

    void highZ(uint8_t contact) const noexcept override;

private:
    const uint8_t startPin;

    uint8_t getInputMode() const noexcept;
};


#endif //CABLE_QA_COMMON_SEQUENTIAL_ADAPTER_H
