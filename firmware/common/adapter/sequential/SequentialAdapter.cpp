//
// Created by ilya on 10/29/17.
//

#include "SequentialAdapter.h"

const unsigned long SequentialAdapter::DEFAULT_TIMEOUT = 50;

SequentialAdapter::SequentialAdapter(const uint8_t startPin, const uint8_t activeLevel,
                                     const unsigned long timeout) noexcept
        : Adapter(activeLevel, timeout), startPin(startPin) {
}

void SequentialAdapter::pulse(uint8_t contact) const noexcept {
    pinMode(getPhysicalPin(contact), OUTPUT);
    auto estimatedLevel = getActiveLevel(SignalLevel::ACTIVE);
    uint8_t physicalPin = getPhysicalPin(contact);
    digitalWrite(physicalPin, estimatedLevel);
    delay(timeout);
    digitalWrite(physicalPin, static_cast<uint8_t>(!estimatedLevel));
}

SignalLevel SequentialAdapter::readLevel(uint8_t contact) const noexcept {
    uint8_t physicalPin = getPhysicalPin(contact);
    pinMode(physicalPin, getInputMode());
    return digitalRead(physicalPin) == activeLevel ? SignalLevel::ACTIVE : SignalLevel::INACTIVE;
}

void SequentialAdapter::writeLevel(uint8_t contact, SignalLevel signalLevel) const noexcept {
    uint8_t physicalPin = getPhysicalPin(contact);
    uint8_t level = getActiveLevel(signalLevel);
    pinMode(physicalPin, OUTPUT);
    digitalWrite(physicalPin, level);
}

bool SequentialAdapter::waitFor(uint8_t contact, SignalLevel signalLevel) const noexcept {
    pinMode(getPhysicalPin(contact), getInputMode());
    auto estimatedLevel = getActiveLevel(signalLevel);
    uint8_t physicalPin = getPhysicalPin(contact);
    auto startTime = millis();
    while (millis() - startTime < timeout) {
        if (digitalRead(physicalPin) == estimatedLevel) {
            return true;
        }
    }
    return false;
}

uint8_t SequentialAdapter::getPhysicalPin(uint8_t contact) const noexcept {
    return startPin + contact;
}

uint8_t SequentialAdapter::getInputMode() const noexcept {
    return static_cast<uint8_t>(activeLevel ? INPUT : INPUT_PULLUP);
}

void SequentialAdapter::highZ(uint8_t contact) const noexcept {
    pinMode(getPhysicalPin(contact), getInputMode());
}
