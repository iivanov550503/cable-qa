//
// Created by ilya on 11/5/17.
//

#ifndef CABLE_QA_COMMON_INDICATOR_H
#define CABLE_QA_COMMON_INDICATOR_H

#include <Arduino.h>
#include "../device/digital/DigitalDeviceBase.h"

class Indicator : public DigitalDeviceBase {
public:
    explicit Indicator(uint8_t activeLevel, uint8_t pin) noexcept;

    void toggle() noexcept;

    void set() noexcept;

    void reset() noexcept;

    uint8_t getState() const noexcept;
private:
    const uint8_t pin;
    volatile uint8_t state;
};


#endif //CABLE_QA_COMMON_INDICATOR_H
