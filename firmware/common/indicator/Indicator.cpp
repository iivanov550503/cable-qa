//
// Created by ilya on 11/5/17.
//

#include "Indicator.h"

Indicator::Indicator(const uint8_t activeLevel, const uint8_t pin) :
        DigitalDeviceBase(activeLevel), pin(pin), state(getActiveLevel(SignalLevel::INACTIVE)) {
}

void Indicator::toggle() {
    state = static_cast<uint8_t>(!state);
    digitalWrite(pin, state);
}

void Indicator::set() {
    state = getActiveLevel(SignalLevel::ACTIVE);
    digitalWrite(pin, state);
}

void Indicator::reset() {
    state = getActiveLevel(SignalLevel::INACTIVE);
    digitalWrite(pin, state);
}

uint8_t Indicator::getState() const {
    return state;
}
