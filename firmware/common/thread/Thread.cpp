//
// Created by ilya on 11/5/17.
//

#include "Thread.h"

void Thread::interrupt() noexcept {
    interrupted = true;
}

bool Thread::isInterrupted() noexcept {
    auto value = interrupted;
    interrupted = false;
    return value;
}
