//
// Created by ilya on 11/5/17.
//

#ifndef CABLE_QA_COMMON_THREAD_H
#define CABLE_QA_COMMON_THREAD_H


class Thread {
public:
    void interrupt() noexcept;

    bool isInterrupted() noexcept;

private:
    volatile bool interrupted = false;
};


#endif //CABLE_QA_COMMON_THREAD_H
