//
// Created by root on 10/27/17.
//

#include "Rj45.h"

const int rj45Contacts = 8;

Rj45::Rj45(Logger *logger, const Adapter *adapter)
        : Rj(logger, adapter, "RJ45", rj45Contacts) {
    patterns.addStraight(getContacts());
    patterns.add(new Pattern("Crossover", "36145278"));
    patterns.addRollover(getContacts());
}

Rj45::Rj45(const Adapter *adapter): Rj(adapter, rj45Contacts) {
}