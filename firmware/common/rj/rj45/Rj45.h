//
// Created by root on 10/27/17.
//

#ifndef CABLE_QA_COMMON_RJ45_H
#define CABLE_QA_COMMON_RJ45_H


#include "../rj.h"

class Rj45: public Rj {
public:
    explicit Rj45(Logger* logger, const Adapter *adapter);

    explicit Rj45(const Adapter *adapter);
};


#endif //CABLE_QA_COMMON_RJ45_H
