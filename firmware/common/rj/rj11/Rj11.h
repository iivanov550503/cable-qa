//
// Created by ilya on 11/2/17.
//

#ifndef COMMON_RJ11_H
#define COMMON_RJ11_H


#include "../rj.h"

class Rj11: public Rj {
public:
    explicit Rj11(Logger *logger, const Adapter *adapter);

    explicit Rj11(const Adapter *adapter);
};


#endif //COMMON_RJ11_H
