//
// Created by ilya on 11/2/17.
//

#include "Rj11.h"

const int rj11Contacts = 4;

Rj11::Rj11(Logger *logger, const Adapter *adapter)
        : Rj(logger, adapter, "RJ11", rj11Contacts) {
    patterns.addStraight(getContacts());
    patterns.add(new Pattern("Straight", "x23x"));
}

Rj11::Rj11(const Adapter *adapter) : Rj(adapter, rj11Contacts) {
}
