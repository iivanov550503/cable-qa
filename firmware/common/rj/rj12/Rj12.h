//
// Created by ilya on 11/2/17.
//

#ifndef COMMON_RJ12_H
#define COMMON_RJ12_H


#include "../rj.h"

class Rj12: public Rj {

public:
    Rj12(Logger *logger, const Adapter *adapter);

    explicit Rj12(const Adapter *adapter);
};


#endif //COMMON_RJ12_H
