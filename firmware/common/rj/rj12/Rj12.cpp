//
// Created by ilya on 11/2/17.
//

#include "Rj12.h"

const int rj12Contacts = 6;

Rj12::Rj12(Logger *logger, const Adapter *adapter) : Rj(logger, adapter, "RJ12", rj12Contacts) {
    patterns.addStraight(getContacts());
    patterns.addRollover(getContacts());
}

Rj12::Rj12(const Adapter *adapter) : Rj(adapter, rj12Contacts) {
}
