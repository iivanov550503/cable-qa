//
// Created by root on 10/27/17.
//

#ifndef CABLE_QA_COMMON_RJ_H
#define CABLE_QA_COMMON_RJ_H

#include "../thread/Thread.h"
#include "../adapter/Adapter.h"
#include "../logger/Logger.h"
#include "patterns/pattern/Pattern.h"
#include "patterns/collection/PatternCollection.h"
#include <ReceiveOnlySoftwareSerial.h>
#include <SendOnlySoftwareSerial.h>

class Rj : public Thread {
public:
    Rj(Logger *logger, const Adapter *adapter, const String &name, int contacts) noexcept;

    explicit Rj(const Adapter* adapter, int contacts) noexcept;

    void test() const noexcept;

    void listen() noexcept;

    String toString() const noexcept;

    int getContacts() const noexcept;

private:
    static const long SERIAL_BAUD_RATE;
    static const unsigned long SERIAL_START_DELAY;
    static const int LISTEN_TIMEOUT;
    static const char BROKEN_SYMBOL;

    const int contacts;
    const String name;
    const Adapter *adapter;

    Logger *logger;

    String testIntegrity() const noexcept;

    String detectType(const String &representation) const noexcept;

    bool hasBreaks(const String& representation) const noexcept;

    bool hasAtLeastTwoBreaks(const String& representation) const noexcept;

    String detectShortCircuit(const String &representation) const noexcept;

    void resetToHighZ() const noexcept;
protected:
    PatternCollection patterns;
};

#endif //CABLE_QA_COMMON_RJ_H
