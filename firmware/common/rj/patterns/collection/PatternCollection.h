//
// Created by ilya on 11/1/17.
//

#ifndef CABLE_QA_COMMON_PATTERN_COLLECTION_H
#define CABLE_QA_COMMON_PATTERN_COLLECTION_H

#include "../pattern/Pattern.h"

class PatternCollection {
public:
    PatternCollection() {
        patterns = new Pattern*[defaultCollectionSize];
        size = defaultCollectionSize;
    }

    virtual ~PatternCollection() {
        for (int i = 0; i < getLength(); ++i) {
            delete patterns[i];
        }
        disposePatterns();
    }

    Pattern* operator[] (int index) const {
        return patterns[index];
    }

    void add(Pattern* pattern) {
        if (length + 1 > size) {
            size *= 2;
            auto temp = new Pattern*[size];
            for (int i = 0; i < getLength(); ++i) {
                temp[i] = patterns[i];
            }
            disposePatterns();
            patterns = temp;
        }
        patterns[length++] = pattern;
    }

    void addStraight(int contacts) {
        String pattern;
        for (int i = 1; i <= contacts; ++i) {
            pattern += String(i);
        }
        add(new Pattern("Straight", pattern));
    }

    void addRollover(int contacts) {
        String pattern;
        for (int i = contacts; i >= 1; --i) {
            pattern += String(i);
        }
        add(new Pattern("Rollover", pattern));
    }

    int getLength() const {
        return length;
    }

private:
    static const int defaultCollectionSize;

    int length = 0;
    int size;
    Pattern** patterns;

    void disposePatterns() {
        delete[] patterns;
    }
};


#endif //CABLE_QA_COMMON_PATTERN_COLLECTION_H
