//
// Created by ilya on 10/30/17.
//

#ifndef CABLE_QA_COMMON_PATTERN_H
#define CABLE_QA_COMMON_PATTERN_H

#include <Arduino.h>

class Pattern {
public:
    Pattern(const String &name, const String &pattern);

    String getName();

    bool isMatch(String wires);
private:
    const String name;
    const String pattern;
};


#endif //CABLE_QA_COMMON_PATTERN_H
