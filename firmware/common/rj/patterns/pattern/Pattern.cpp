//
// Created by ilya on 10/30/17.
//

#include "Pattern.h"

Pattern::Pattern(const String &name, const String &pattern) : name(name), pattern(pattern) {}

String Pattern::getName() {
    return name;
}

bool Pattern::isMatch(String wires) {
    return wires == pattern;
}
