//
// Created by root on 10/27/17.
//

#include "rj.h"

const long Rj::SERIAL_BAUD_RATE = 9600;
const unsigned long Rj::SERIAL_START_DELAY = 100;
const int Rj::LISTEN_TIMEOUT = 5000;
const char Rj::BROKEN_SYMBOL = 'x';

Rj::Rj(Logger *logger, const Adapter *adapter, const String &name, const int contacts) noexcept
        : contacts(contacts), adapter(adapter),
          name(name), logger(logger) {
}

Rj::Rj(const Adapter *adapter, const int contacts) noexcept
        : contacts(contacts), adapter(adapter) {
}

String Rj::toString() const noexcept {
    return name + " (" + String(contacts) + " pins)";
}

int Rj::getContacts() const noexcept {
    return contacts;
}

void Rj::test() const noexcept {
    logger->wake();
    logger->clear();
    logger->println(toString());
    logger->println();

    auto representation = testIntegrity();
    logger->println(representation);
    if (hasBreaks(representation)) {
        if (hasAtLeastTwoBreaks(representation)) {
            auto shortCircuit = detectShortCircuit(representation);
            if (shortCircuit.length()) {
                logger->println("SC:" + shortCircuit);
            }
        }
    }
    logger->println("Type:" + detectType(representation));
    resetToHighZ();
}

String Rj::testIntegrity() const noexcept {
    char wires[contacts + 1] = {};
    memset(wires, BROKEN_SYMBOL, sizeof(wires));
    wires[contacts] = '\0';

    for (uint8_t i = 0; i < contacts; ++i) {
        adapter->pulse(i);
        ReceiveOnlySoftwareSerial serial = ReceiveOnlySoftwareSerial(adapter->getPhysicalPin(i));
        serial.begin(SERIAL_BAUD_RATE);
        delay(SERIAL_START_DELAY);
        if (serial.available()) {
            wires[i] = serial.read();
        }
    }

    return String(wires);
}

String Rj::detectType(const String &representation) const noexcept {
    for (int i = 0; i < patterns.getLength(); ++i) {
        if (patterns[i]->isMatch(representation)) {
            return patterns[i]->getName();
        }
    }
    return "Unknown";
}

bool Rj::hasBreaks(const String &representation) const noexcept {
    return representation.indexOf(BROKEN_SYMBOL) != -1;
}

bool Rj::hasAtLeastTwoBreaks(const String &representation) const noexcept {
    return representation.indexOf(BROKEN_SYMBOL) != representation.lastIndexOf(BROKEN_SYMBOL);
}

String Rj::detectShortCircuit(const String &representation) const noexcept {
    String sc;
    for (uint8_t i = 0; i < representation.length(); ++i) {
        auto iRepresentation = String(i + 1);
        if (representation[i] != BROKEN_SYMBOL ||
            sc.indexOf(iRepresentation) != -1)
            continue;
        adapter->writeLevel(i, SignalLevel::ACTIVE);
        for (auto j = static_cast<uint8_t>(i + 1); j < representation.length(); ++j) {
            if (representation[j] != BROKEN_SYMBOL) continue;
            if (adapter->readLevel(j) == SignalLevel::ACTIVE) {
                if (sc.indexOf(iRepresentation) == -1) {
                    if (sc.length() > 0) {
                        sc.concat(',');
                    }
                    sc.concat(iRepresentation);
                }
                sc.concat(j + 1);
            }
        }
    }
    return sc;
}

void Rj::resetToHighZ() const noexcept {
    for (uint8_t i = 0; i < contacts; ++i) {
        adapter->highZ(i);
    }
}

void Rj::listen() noexcept {
    auto counter = contacts;
    auto startTime = millis();
    while (counter > 0 && millis() - startTime < LISTEN_TIMEOUT && !isInterrupted()) {
        for (uint8_t i = 0; i < contacts; ++i) {
            if (adapter->readLevel(i) != SignalLevel::ACTIVE) continue;
            adapter->waitFor(i, SignalLevel::INACTIVE);
            delay(SERIAL_START_DELAY);
            SendOnlySoftwareSerial serial = SendOnlySoftwareSerial(adapter->getPhysicalPin(i));
            serial.begin(SERIAL_BAUD_RATE);
            serial.print(i + 1);
            --counter;
        }
    }
    resetToHighZ();
}
