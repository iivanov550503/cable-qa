//
// Created by ilya on 10/29/17.
//

#ifndef CABLE_QA_COMMON_LCD_LOGGER_H
#define CABLE_QA_COMMON_LCD_LOGGER_H


#include "../Logger.h"
#include <LCD5110_Basic.h>

extern uint8_t SmallFont[];

class LCDLogger: public Logger {
public:
    explicit LCDLogger(LCD5110 *lcd);

    void initialize() override;

    void sleep() override;

    void wake() override;

    void clear() override;

    void println() override;

    void println(const String &message) override;
private:
    static const int CONTRAST;
    static const int LINES;
    static const int LINE_SCALE_FACTOR;

    LCD5110 *lcd;
    int line = 0;

    void incrementLine() {
        if (line + 1 >= LINES) {
            line = 0;
            return;
        }
        ++line;
    }
};


#endif //CABLE_QA_COMMON_LCD_LOGGER_H
