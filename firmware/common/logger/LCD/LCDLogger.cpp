//
// Created by ilya on 10/29/17.
//

#include "LCDLogger.h"

const int LCDLogger::CONTRAST = 50;
const int LCDLogger::LINES = 6;
const int LCDLogger::LINE_SCALE_FACTOR = 8;

void LCDLogger::clear() {
    lcd->clrScr();
    line = 0;
}

void LCDLogger::println() {
    incrementLine();
}

void LCDLogger::println(const String &message) {
    lcd->print(message, LEFT, line * LINE_SCALE_FACTOR);
    println();
}

LCDLogger::LCDLogger(LCD5110 *lcd) : lcd(lcd) {
}

void LCDLogger::sleep() {
    lcd->enableSleep();
}

void LCDLogger::wake() {
    lcd->disableSleep();
}

void LCDLogger::initialize() {
    lcd->InitLCD(CONTRAST);
    lcd->setFont(SmallFont);
}
