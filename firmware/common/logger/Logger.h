//
// Created by root on 10/27/17.
//

#ifndef CABLE_QA_COMMON_LOGGER_H
#define CABLE_QA_COMMON_LOGGER_H

#include <Arduino.h>

class Logger {
public:
    virtual void initialize() {};
    virtual void sleep() {};
    virtual void wake() {};
    virtual void clear() = 0;
    virtual void println() = 0;
    virtual void println(const String& message) = 0;
};


#endif //CABLE_QA_COMMON_LOGGER_H
