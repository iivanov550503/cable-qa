#define MASTER

#include <LCD5110_Basic.h>
#include <LCDLogger.h>
#include <Bounce2.h>
#include <SequentialAdapter.h>
#include <ReceiveOnlySoftwareSerial.h>
#include <Rj11.h>
#include <Rj12.h>
#include <Rj45.h>
#include <Indicator.h>
#include <Switch.h>

const int BUTTON_PIN = A0;
const int INDICATOR_PIN = 13;
const int SWITCH_PIN = A5;

const int startPin = 0;

Rj *currentRj;
Bounce button = Bounce();
LCD5110 lcd(8, 9, 10, 11, 12);
LCDLogger logger = LCDLogger(&lcd);
SequentialAdapter adapter = SequentialAdapter(startPin, LOW);
Rj11 rj11 = Rj11(&logger, &adapter);
Rj12 rj12 = Rj12(&logger, &adapter);
Rj45 rj45 = Rj45(&logger, &adapter);
Indicator green = Indicator(HIGH, INDICATOR_PIN);
Switch modeSwitch = Switch(SWITCH_PIN, 3);

void setup() {
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(INDICATOR_PIN, OUTPUT);

  button.attach(BUTTON_PIN);
  button.interval(10);

  logger.initialize();
}

void loop() {
  if (button.update() && button.fell()) {
    green.set();

    if (green.getState()) {
      switch (modeSwitch.getMode()) {
        case 0:
          currentRj = &rj45;
          break;
        case 1:
          currentRj = &rj12;
          break;
        default:
          currentRj = &rj11;
      }
      currentRj->test();
      green.reset();
    }
  }
}
