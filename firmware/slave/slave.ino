#define SLAVE

#include <SequentialAdapter.h>
#include <SendOnlySoftwareSerial.h>
#include <Rj11.h>
#include <Rj12.h>
#include <Rj45.h>
#include <Indicator.h>
#include <Switch.h>

const int BUTTON_PIN = 2;
const int INDICATOR_PIN = 13;
const int SWITCH_PIN = A5;

const int startPin = 3;

Rj *currentRj;
SequentialAdapter adapter = SequentialAdapter(startPin, LOW);
Rj11 rj11 = Rj11(&adapter);
Rj12 rj12 = Rj12(&adapter);
Rj45 rj45 = Rj45(&adapter);
Switch modeSwitch = Switch(SWITCH_PIN, 3);
Indicator green = Indicator(HIGH, INDICATOR_PIN);

void click_button() {
  if (!green.getState()) {
    green.set();
    return;
  }
  currentRj->interrupt();
}

void setup() {
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(INDICATOR_PIN, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), click_button, FALLING);
}

void loop() {
  if (green.getState()) {
    switch (modeSwitch.getMode()) {
      case 0:
        currentRj = &rj45;
        break;
      case 1:
        currentRj = &rj12;
        break;
      default:
        currentRj = &rj11;
    }
    currentRj->listen();
    green.reset();
  }
}
